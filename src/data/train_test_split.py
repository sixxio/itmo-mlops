import click
import pandas as pd
from sklearn.model_selection import train_test_split


@click.command()
@click.option('--input-file', required=True, type=click.Path(exists=True), help='Path to the input Parquet file')
@click.option(
    '--train-file', required=True, type=click.Path(), help='Path to the output Parquet file for the training data'
)
@click.option('--test-file', required=True, type=click.Path(), help='Path to the output Parquet file for the test data')
def split_data(input_file, train_file, test_file):
    """
    Perform a train-test split on a Parquet file and save the results to six separate Parquet files.

    This script reads a Parquet file, splits the data into training, testing, and validation sets,
    and writes the results (features and targets together) to two separate Parquet files.
    """
    # Read the input Parquet file
    df = pd.read_parquet(input_file)

    # Split the data into training, testing, and validation sets
    train, test = train_test_split(df, test_size=0.2, random_state=42)

    # Write the data to Parquet files
    train.to_parquet(train_file, index=False)
    test.to_parquet(test_file, index=False)

    click.echo(f'Training data written to: {train_file}')
    click.echo(f'Test data written to: {test_file}')


if __name__ == '__main__':
    split_data()
