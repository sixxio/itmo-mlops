import click
import pandas as pd


@click.command()
@click.option('--input-file', default='data/intermediate/input.parquet', help='Path to the input Parquet file')
@click.option(
    '--output-file', default='data/intermediate/dropped_consts.parquet', help='Path to the output Parquet file'
)
def drop_constant_columns(input_file, output_file):
    """
    Drop constant columns from a Parquet file.
    """
    df = pd.read_parquet(input_file)
    df = df.drop(columns=[i for i in df.columns if df[i].max() == df[i].min()])

    # Save the DataFrame with constant columns dropped to the output Parquet file
    df.to_parquet(output_file, index=False)
    print(f"Data with constant columns dropped saved to {output_file}")


if __name__ == '__main__':
    drop_constant_columns()
