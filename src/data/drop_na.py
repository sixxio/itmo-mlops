import click
import pandas as pd


@click.command()
@click.option('--input-file', default='data/intermediate/input.parquet', help='Path to the input Parquet file')
@click.option('--output-file', default='data/intermediate/dropped_na.parquet', help='Path to the output Parquet file')
def drop_na_rows(input_file, output_file):
    """
    Drop rows with at least one NaN value from a Parquet file.
    """
    df = pd.read_parquet(input_file)

    # Drop rows with at least one NaN value
    df = df.dropna(how='any')

    # Save the DataFrame with NaN rows dropped to the output Parquet file
    df.to_parquet(output_file, index=False)
    print(f"Data with NaN rows dropped saved to {output_file}")


if __name__ == '__main__':
    drop_na_rows()
