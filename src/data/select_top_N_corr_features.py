import click
import pandas as pd
import json


@click.command()
@click.option('--input-file', default='data/raw/data.parquet', help='Path to the input Parquet file')
@click.option('--output-file', default='data/intermediate/top_25_features.json', help='Path to the output JSON file')
@click.option('--n', default=25, help='Number of top features to select')
def select_top_features(input_file, output_file, n):
    """
    Select the top N features based on correlation with the label.
    """
    df = pd.read_parquet(input_file)
    top_N_features = df.corr().abs()['Label'].sort_values(ascending=False).index[1:].tolist()[:n]

    # Save the top N features to the output JSON file
    json.dump(top_N_features, open(output_file, 'w'))
    print(f"Top {n} features saved to {output_file}")


if __name__ == '__main__':
    select_top_features()
