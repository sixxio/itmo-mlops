import click
import pandas as pd
import json


@click.command()
@click.option('--input-file', default='data/intermediate/processed_df.parquet', help='Path to the input Parquet file')
@click.option(
    '--features-file',
    default='data/intermediate/top_N_features.json',
    help='Path to the JSON file containing the top N features',
)
@click.option('--output-file', default='data/intermediate/data.parquet', help='Path to the output Parquet file')
def drop_unneeded_features(input_file, features_file, output_file):
    """
    Drop unneeded features from a Parquet file.
    """
    df = pd.read_parquet(input_file)
    top_N_features = json.load(open(features_file))

    # Save the DataFrame with only the top N features to the output Parquet file
    df[top_N_features + ['Label']].to_parquet(output_file, index=False)
    print(f"Data with only the top {len(top_N_features)} features saved to {output_file}")


if __name__ == '__main__':
    drop_unneeded_features()
