import click
import mlflow
import pandas as pd
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.metrics import precision_score, recall_score, f1_score, accuracy_score

import os

os.environ['MLFLOW_TRACKING_URI'] = 'postgresql+psycopg2://postgres:postgres@192.168.3.3:10002/mlflow'
os.environ['MLFLOW_S3_ENDPOINT_URL'] = "http://192.168.3.3:10000"
os.environ['AWS_ACCESS_KEY_ID'] = 'sixxio'
os.environ['AWS_SECRET_ACCESS_KEY'] = 'sixxio1234'
mlflow.set_tracking_uri("http://192.168.3.3:10003")


@click.command()
@click.option('--train-file', required=True, type=click.Path(exists=True), help='Path to the training Parquet file')
@click.option('--test-file', required=True, type=click.Path(exists=True), help='Path to the test Parquet file')
def train_model(train_file, test_file):
    """
    Train a Random Forest Regressor model and evaluate its performance.

    This script reads training and test data from Parquet files, trains a Random Forest Regressor model,
    and writes the mean absolute error and mean squared error to the specified output file.
    """
    train = pd.read_parquet(train_file)
    test = pd.read_parquet(test_file)

    x_train, y_train = train.drop(columns='Label'), train['Label']
    x_test, y_test = test.drop(columns='Label'), test['Label']

    exp_id = mlflow.create_experiment('Default experiment', artifact_location="s3://mlflow")
    with mlflow.start_run(run_name='Default run', experiment_id=exp_id):

        model = GradientBoostingClassifier(
            learning_rate=0.1,
            max_depth=5,
            max_features='log2',
            min_samples_leaf=2,
            min_samples_split=10,
            n_estimators=200,
            random_state=42,
        )

        model.fit(x_train, y_train)
        y_pred = model.predict(x_test)
        accuracy = accuracy_score(y_test, y_pred)
        precision = precision_score(y_test, y_pred)
        recall = recall_score(y_test, y_pred)
        f1 = f1_score(y_test, y_pred)

        mlflow.log_metric("accuracy", accuracy)
        mlflow.log_metric("precision", precision)
        mlflow.log_metric("recall", recall)
        mlflow.log_metric("f1_score", f1)

        mlflow.sklearn.log_model(
            model, artifact_path="gradient-boosting-model", registered_model_name="gradient-boosting-model"
        )

    print(f'Accuracy: {accuracy:.4f}')
    print(f'Precision: {precision:.4f}')
    print(f'Recall: {recall:.4f}')
    print(f'F1-score: {f1:.4f}')


if __name__ == '__main__':
    train_model()
